﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;

namespace CourseWork
{
    class grep
    {
        static void Main()
        {
            Console.Title = @"Эмулятор команды grep";
            //Логические переменные, хранящие выбранные пользователем опции программы
            bool optionC, optionI, optionN;//optionC - выводить только кол-во совпадений, optionI - не различать регистр, optionN - выводить номер строки
            FileInfo source;
            while (true)
            {
                string pathToFile = @"Test.txt";//Путь до файла для поиска по умолчанию
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Введите полный путь к файлу, в котором будет осуществляться поиск.\nДля поиска в файле по умолчанию нажмите Enter.");
                Console.ForegroundColor = ConsoleColor.White;
                string line = @Console.ReadLine();
                //Если входная строка не была пустой, заменить путь до файла для поиска
                if (line != "")
                    pathToFile = line;
                try
                {
                    //Создать объект, хранящий информацию о файле
                    source = new FileInfo(pathToFile);
                    //Сгенерировать исключение, если файл не существует или не является текстовым
                    if ((source.Extension != @".txt") || (!source.Exists))
                        throw new Exception(@"Файл не существует или является не .txt-файлом!");
                    break;
                }
                catch (Exception ex)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Ошибка при открытии файла!\n" + ex.Message);
                    Console.ReadKey();
                    Console.ForegroundColor = ConsoleColor.White;
                }
            }
            while (true)
            {
                //Сбросить значения опций, передаваемых программе
                optionC = optionI = optionN = false;
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Введите команду grep с необходимыми опциями[-c, -i, -n].\nДля выхода введите 0.");
                Console.ForegroundColor = ConsoleColor.White;
                string str = Console.ReadLine();
                //Разделить входную строку по пробелам и записать в массив
                string[] commands = str.Split(' ');
                //Осуществить выход из программы, если первым словом входной строки было "0"
                if (commands[0] == "0")
                    break;
                //Перейти на след. проход бесконечного цикла, если входная строка была пустой или первым словом не было "grep" 
                if ((str == "") || (str == null) || (commands[0] != "grep"))
                    continue;
                //Включить опции, если соответствующие им слова были сохранены в массиве с первого по предпоследний элементы
                for (int i = 1; i < commands.Length - 1; i++)
                {
                    if (commands[i] == "-c")
                        optionC = true;
                    if (commands[i] == "-i")
                        optionI = true;
                    if (commands[i] == "-n")
                        optionN = true;
                }
                FindPattern(source, commands[commands.Length - 1], optionC, optionI, optionN);
                Console.WriteLine("Для продолжения нажмите любую клавишу...");
                Console.ReadKey();
            }
        }
        //Метод, осуществляющий поиск вхождения шаблона (string pattern) в текстовый файл (FileInfo source)
        static void FindPattern(FileInfo source, string pattern, bool c, bool i, bool n)
        {
            int lineCounter = 1;//Счетчик строк в файле
            int matchCounter = 0;//Счетчик успешных вхождений шаблона
            string line;
            Regex rx;
            try
            {
                //Создать объект регулярного выражения, содержащий шаблон для поиска
                if (i)
                    rx = new Regex(pattern, RegexOptions.IgnoreCase);//...без учета регистра, если была введена соответствующая опция
                else
                    rx = new Regex(pattern);
                //Создать объект потока для чтения из текстового файла
                StreamReader sr = new StreamReader(source.FullName, Encoding.GetEncoding(1251));
                //Выполнить построчное чтение файла
                do
                {
                    line = sr.ReadLine();
                    //Создать коллекцию из успешных вхождений шаблона в строку файла
                    MatchCollection matches = rx.Matches(line);
                    if (!c)
                    {
                        if (n)
                        {
                            //Если не выбран режим "только кол-во совпадений" и выбрана опция вывода номера строки
                            foreach (Match match in matches)
                            {
                                //...вывести на экран каждое совпадение из коллекции в такой форме
                                string result = "В строке №" + lineCounter.ToString() + " найдено соответствие: " + match.Value;
                                Console.WriteLine(result + "\nОригинальная строка: " + line + "\n");
                            }
                        }
                        //Если не выбран режим "только кол-во совпадений" и не выбрана опция вывода номера строки
                        else
                        {
                            foreach (Match match in matches)
                            {
                                //...вывести на экран каждое совпадение из коллекции в такой форме
                                string result = "Найдено соответствие: " + match.Value;
                                Console.WriteLine(result + "\nВ строке: " + line + "\n");
                            }

                        }
                    }
                    //Увеличить счетчик вхождений на кол-во элементов в коллекции успешных совпадений
                    matchCounter += matches.Count;
                    //Инкрементировать счетчик строк
                    lineCounter++;
                }
                while (!sr.EndOfStream);
                sr.Close();
                //Вывести на экран общее кол-во успешных вхождений шаблона
                Console.WriteLine("Всего найдено {0:d} соответствий.", matchCounter);
            }
            //Осуществить отдельный перехват исключения недопустимого значения
            catch (ArgumentException ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Ошибка в регулярном выражении!\n" + ex.Message);
                Console.ForegroundColor = ConsoleColor.White;
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Ошибка!\n" + ex.Message);
                Console.ForegroundColor = ConsoleColor.White;
            }
        }
    }
}
